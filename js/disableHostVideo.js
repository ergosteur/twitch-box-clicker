//wait 5 sec before starting, only start if one level deep URL path
setTimeout(() => {
	if (window.location.pathname.split('/').length == 2) {
		switchHomeLayout(); 
	}
}, 5000);

//hack in function to disable new Twitch home layout and remove hosting video
function switchHomeLayout() {
	if (document.getElementsByClassName('channel-root--home').length > 0) {
		var pathArray = window.location.pathname.split('/');
		var selectorString = "a.tw-interactive[href='/" + pathArray[1] + "']";
		var channelNameLink = document.querySelectorAll(selectorString);
		var i;
		for (i = 0; i < channelNameLink.length; i++) {
			if (channelNameLink[i].innerText.toLowerCase() == pathArray[1]) {
				channelNameLink[i].click();
				break;
			}
		} 
		console.log('New home layout detected, waiting for layout switch...');
		setTimeout(() => {  removeVideo(); }, 10000);
	}
	else { 
		setTimeout(() => {  removeVideo(); }, 5000)
	}
}
function removeVideo() {
	var streamTitleSelector = '[data-a-target="stream-title"';
	var streamTitle = document.querySelectorAll(streamTitleSelector);
	if (streamTitle[0].innerText.startsWith("hosting")) {
		videos = document.getElementsByTagName('video');
		videos[0].parentNode.removeChild(videos[0]);
		console.log('Removed hosted video element')
	}
}